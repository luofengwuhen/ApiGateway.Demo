﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiServiceB.Controllers
{
    [ApiController]
    //[Route("[controller]")]
    [Route("api")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> GetAsync()
        {
            /*  var rng = new Random();
              return Enumerable.Range(1, 5).Select(index => new WeatherForecast
              {
                  Date = DateTime.Now.AddDays(index),
                  TemperatureC = rng.Next(-20, 55),
                  Summary = Summaries[rng.Next(Summaries.Length)]
              })
              .ToArray();*/
            Console.WriteLine(nameof(ApiServiceB) + $"time is :{DateTime.Now}");

            var result = await Task.Run(() =>
            {
                return $"This is from {HttpContext.Request.Host.Value}, path: {HttpContext.Request.Path}, time is :{DateTime.Now}";
            });
            return Ok(result);
        }
    }
}
